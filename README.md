# Recovery

When the application starts a supervisor containing an Agent and a GenServer get started as well.

The GenServer is responsible for periodically retrieving data from an API. After the data is retrieved it gets sent to a LiveView page through PubSub.
This LiveView page fetches the latest data on page load, and gets pushed new data when it becomes available, replacing the old data.
This LiveView is also able to change the interval where data gets retrieved.

On a set interval a timebomb in the GenServer process makes it crash. When the GenServer gets restarted it retrieves the latest retrieved state from an Agent running in the same supervisor.
