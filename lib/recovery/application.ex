defmodule Recovery.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      RecoveryWeb.Telemetry,
      Recovery.Repo,
      {DNSCluster, query: Application.get_env(:recovery, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Recovery.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: Recovery.Finch},
      # Start a worker by calling: Recovery.Worker.start_link(arg)
      Recovery.ApiWorkerSupervisor,
      # Start to serve requests, typically the last entry
      RecoveryWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Recovery.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    RecoveryWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
