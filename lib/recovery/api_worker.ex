defmodule Recovery.ApiWorker do
  use GenServer
  use Tesla

  require Logger

  @default_interval 10_000
  @timebomb_interval 15_000

  plug Tesla.Middleware.BaseUrl, "https://v2.jokeapi.dev/joke/Programming"
  plug Tesla.Middleware.JSON

  # Client
  def start_link(_) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def get_latest_data, do: :sys.get_state(__MODULE__)[:data]

  def update_interval(interval) do
    GenServer.cast(__MODULE__, {:change_interval, interval})
  end

  # Server
  def init(_opts) do
    {:ok, tref} = :timer.send_interval(@default_interval, :get_data)
    {:ok, _} = :timer.send_interval(@timebomb_interval, :timebomb)

    data = Recovery.ApiWorkerStash.get()

    {:ok, %{tref: tref, data: data}}
  end

  def handle_info(:get_data, state) do
    with {:ok, %{body: body}} <- get("") do
      Phoenix.PubSub.broadcast!(Recovery.PubSub, "data", %{event: :get_data, data: body})
      Recovery.ApiWorkerStash.update(body)

      {:noreply, %{state | data: body}}
    else
      _ -> {:noreply, state}
    end
  end

  def handle_info(:timebomb, _state), do: raise("timebomb triggered")

  def handle_cast({:change_interval, interval}, %{tref: tref} = state) do
    new_tref =
      with {:ok, :cancel} <- :timer.cancel(tref) do
        {:ok, tref} = :timer.send_interval(interval, :get_data)
        tref
      else
        _ ->
          tref
      end

    {:noreply, %{state | tref: new_tref}}
  end
end
