defmodule Recovery.ApiWorkerStash do
  use Agent

  def start_link(_), do: Agent.start_link(fn -> nil end, name: __MODULE__)

  def get, do: Agent.get(__MODULE__, & &1)
  def update(data), do: Agent.update(__MODULE__, fn _state -> data end)
end
