defmodule Recovery.ApiWorkerSupervisor do
  use Supervisor

  def start_link(_init_arg) do
    Supervisor.start_link(__MODULE__, [])
  end

  def init([]) do
    children = [
      Recovery.ApiWorkerStash,
      Recovery.ApiWorker
    ]

    opts = [strategy: :rest_for_one, name: __MODULE__]
    Supervisor.init(children, opts)
  end
end
