defmodule RecoveryWeb.PageHTML do
  use RecoveryWeb, :html

  embed_templates "page_html/*"
end
