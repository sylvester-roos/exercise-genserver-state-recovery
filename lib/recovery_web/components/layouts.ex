defmodule RecoveryWeb.Layouts do
  use RecoveryWeb, :html

  embed_templates "layouts/*"
end
