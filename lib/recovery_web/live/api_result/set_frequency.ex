defmodule RecoveryWeb.ApiResult.SetFrequency do
  use RecoveryWeb, :live_component

  @ms_to_seconds 1000

  def render(assigns) do
    ~H"""
    <div class="mt-2">
      <.form for={@form} phx-submit="save" phx-target={@myself}>
        <.input type="number" field={@form[:frequency]} min="1" max="60" />
        <button class="mt-2 px-4 py-2 bg-cyan-500 text-white rounded-full" type="submit">
          Set frequency
        </button>
      </.form>
    </div>
    """
  end

  def mount(socket) do
    # Initialize the form with an empty map for frequency
    {:ok, assign(socket, form: to_form(%{"frequency" => ""}))}
  end

  def handle_event("save", %{"frequency" => frequency}, socket) do
    # Handle the form submission, e.g., save the frequency value
    frequency
    |> String.to_integer()
    |> Kernel.*(@ms_to_seconds)
    |> set_frequency()

    {:noreply, socket}
  end

  defp set_frequency(frequency), do: Recovery.ApiWorker.update_interval(frequency)
end
