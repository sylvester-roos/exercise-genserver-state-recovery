defmodule RecoveryWeb.ApiResult.DisplayData do
  use Phoenix.Component

  def data(assigns) do
    assigns |> dbg()

    ~H"""
    <div class="mt-2">
      <%= case @data["type"] do %>
        <% "single" -> %>
          <%= single(assigns) %>
        <% "twopart" -> %>
          <%= twopart(assigns) %>
        <% _ -> %>
          <p>Waiting for data...</p>
      <% end %>
    </div>
    """
  end

  defp single(assigns) do
    ~H"""
    <p>
      <%= @data["joke"] %>
    </p>
    """
  end

  defp twopart(assigns) do
    ~H"""
    <p>
      <%= @data["setup"] %>
    </p>
    <br />
    <p>
      <%= @data["delivery"] %>
    </p>
    """
  end
end
