defmodule RecoveryWeb.ApiResult do
  use RecoveryWeb, :live_view

  alias RecoveryWeb.ApiResult.{SetFrequency, DisplayData}

  def render(assigns) do
    ~H"""
    <code>
      <p>Debug:</p>
      <p>data: <%= @data |> inspect() %></p>
    </code>

    <.live_component module={SetFrequency} id="set_frequency" />

    <%= DisplayData.data(assigns) %>
    """
  end

  def mount(_params, _session, socket) do
    Phoenix.PubSub.subscribe(Recovery.PubSub, "data")

    data = Recovery.ApiWorker.get_latest_data()

    socket = assign(socket, %{data: data})
    {:ok, socket}
  end

  # receive data from api_worker broadcast
  def handle_info(%{event: :get_data, data: data}, socket) do
    {:noreply, assign(socket, :data, data)}
  end
end
